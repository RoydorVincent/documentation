# Resources
Red_Hat_JBoss_Enterprise_Application_Platform-7.0-Configuring_Messaging-en-US.pdf
> Contient les informations détaillé de la configuration de JMS.  
  Contient aussi certains conseil d'amélioraton des performances.

# Terme utilisé
* `<JBOSS_HOME>` : emplacement contenat le dossier bin une fois l'archive de JBoss EAP 7 extraite.  
* `<JBOSS_DOMAIN_HOME>` : emplacement contenant le dossier de base du domaine. Emplacement séparrer afin de protégé de l'écrasement lors de mise a jour.  

# CLI
## Possibilité
* voir l'historique des changement dans la conf  
* gérer les niveau de logs  
* gérer le serveur même si il est éteint  

# CLUSTER
HA => High Availability  
## HA
* 2 types  
 * fail-over : permet la connection sur l'application même si un noeud crash  
  * partage la session HTTP, les EJBs et les SSO crédenttials  
 * load balancing : permet un répartition de charge  
  * utilisation grand nombre de requette  
### HA Singleton  
* instance unique au sein du cluster  

# Ajout users 
 ## Utilisateur d'administration
On ajout un utilisateur de management (a. Management User) avec `<JBOSS_HOME>/bin/add-user.sh` sur chaque domaine controleur.  
Dans mon exemple, je crée un utilisteur identifié par `admin` et avec un password de `admin!`  
```bash
/opt/jboss/jboss-eap-7.0-active/bin/add-user.sh admin passAdmin!
```
<!-- 
dans le cas ou vous créer un domaine, à la question se connecter à un autre AS serveur veilliez a répondre : oui  
Notez bien l'identifiant qui vous est donné. Dans mon exemple c'est : `<secret value="THVuMzc3MyQ=" />`  
-->
 ## Utilisateur de noeud
 Les autres noeuds doivent pouvoir se connecter sur le domain controleur.  
 
 Je crée donc sur le domaine contronleur du node1 les users suivants :  
 
 | user  | password   |  
 |-------|------------|  
 | node2 | passNode2! |  
 | node3 | passNode3! |  
 ```bash
/opt/jboss/jboss-eap-7.0-active/bin/add-user.sh

Quel type d'utilisateur souhaitez-vous ajouter ?
 a) Management User (mgmt-users.properties)
 b) Application User (application-users.properties)
(a): a

Saisir les informations sur le nouvel utilisateur
Utiliser le domaine 'ManagementRealm' selon les fichiers de propriétés existants.
Nom d'utilisateur : node2
Les recommandations de mot de passe sont énumérés ci-dessous. Pour modifier ces restrictions, modifier le fichier de configuration add-user.properties.
 - Le mot de passe doit être différent du nom d'utilisateur
 - Le mot de passe doit correspondre à une des valeurs limitées suivantes {root, admin, administrator}
 - Le mot de passe doit contenir au moins 8 caractères, 1 caractère(s) alphabétique(s), 1 chiffre (s), 1 symbole(s) non alpha-numériques
Mot de passe :
Saisir mot de passe à nouveau :
Quels groupes souhaitez-vous impartir à cet utilisateur ? (Veuillez saisir une liste séparée par des virgules, ou laisser vide)[  ]:
L'utilisateur 'node2' va être ajouté pour le domaine 'ManagementRealm'
Est-ce correct ? oui/non? oui
Utilisateur 'node2' ajouté au fichier '/opt/jboss/jboss-eap-7.0-active/domain/configuration/mgmt-users.properties'
Utilisateur 'node2' ajouté aux groupes  dans le fichier '/opt/jboss/jboss-eap-7.0-active/domain/configuration/mgmt-groups.properties'
Est-ce que ce nouvel utilisateur va être utilisé pour qu'un processus AS puisse se connecter à un autre processus AS, comme par exemple
 pour qu'un contrôleur d'hôte esclave se connecte au master ou pour une connexion distante de serveur à serveur pour les appels EJB.
oui/non ? oui
Pour représenter l'utilisateur, ajouter ce qui suit à la définition des identités du serveur <secret value="cGFzc05vZGUyIQ==" />
 ```  
 ```bash
   /opt/jboss/jboss-eap-7.0-active/bin/add-user.sh
   
   Quel type d'utilisateur souhaitez-vous ajouter ?
    a) Management User (mgmt-users.properties)
    b) Application User (application-users.properties)
   (a): a
   
   Saisir les informations sur le nouvel utilisateur
   Utiliser le domaine 'ManagementRealm' selon les fichiers de propriétés existants.
   Nom d'utilisateur : node3
   Les recommandations de mot de passe sont énumérés ci-dessous. Pour modifier ces restrictions, modifier le fichier de configuration add-user.properties.
    - Le mot de passe doit être différent du nom d'utilisateur
    - Le mot de passe doit correspondre à une des valeurs limitées suivantes {root, admin, administrator}
    - Le mot de passe doit contenir au moins 8 caractères, 1 caractère(s) alphabétique(s), 1 chiffre (s), 1 symbole(s) non alpha-numériques
   Mot de passe :
   Saisir mot de passe à nouveau :
   Quels groupes souhaitez-vous impartir à cet utilisateur ? (Veuillez saisir une liste séparée par des virgules, ou laisser vide)[  ]:
   L'utilisateur 'node3' va être ajouté pour le domaine 'ManagementRealm'
   Est-ce correct ? oui/non? oui
   Utilisateur 'node3' ajouté au fichier '/opt/jboss/jboss-eap-7.0-active/domain/configuration/mgmt-users.properties'
   Utilisateur 'node3' ajouté aux groupes  dans le fichier '/opt/jboss/jboss-eap-7.0-active/domain/configuration/mgmt-groups.properties'
   Est-ce que ce nouvel utilisateur va être utilisé pour qu'un processus AS puisse se connecter à un autre processus AS, comme par exemple
    pour qu'un contrôleur d'hôte esclave se connecte au master ou pour une connexion distante de serveur à serveur pour les appels EJB.
   oui/non ? oui
   Pour représenter l'utilisateur, ajouter ce qui suit à la définition des identités du serveur <secret value="cGFzc05vZGUzIQ==" />
   ```
 Et sur le domaine controleur node2 :  
 
  | user  | password   |  
  |-------|------------|  
  | node1 | passNode1! |  
  | node3 | passNode3! |  
  
-----------------------------------------------------------------------------------------------------------------------------  
  
# Installation d'un serveur JBoss
Après avoir téléchargé le zip, il suffit de l'extraire a l'emplacement voulu. Cet emplacement sera référencé par la suite par `<JBOSS_HOME>`  
Deux possibilité d'utilisation s'offre:  
## Standalone  
En principe on installe un serveur complet qui sera indépendant dans sa configuration.  
### Démarage  
`<JBOSS_HOME>/bin/standalone.sh`  

## Domain  
*(voir laptop-vroydor/C:/Users/vroydor/Documents/Documentation/JBoss/EAP 7/Red_Hat_JBoss_Enterprise_Application_Platform-7.0-Configuration_Guide-en-US.pdf §.8)*
  
Le mode domaine permet d'associer des serveurs JBoss dans des groupes.  
Le domaine possède :
* un controleur de domaine
* un controleur de domaine de backup (Optionnel)
* un ou plusiers host controller
C'est ici que la configuration du domaine est faite.  
Chaque Host controleur s'inscrit au près du controleur de domaine. Un host peux possèder 0 ou plusieur serveur-group.

Le fichier de configuration du domaine "domain.xml" se trouve dans `<JBOSS_HOME>/domain/configuration`

Le controleur de domaine doit démarrer avant les serveurs controleur d'hote.


### Voici un exemple de configuration d'un domaine JBoss EAP 7
[ShemaArchiServeurs]: resources/JBossEAP7_DomainCluster.png "Schema : JBoss installation domain pour cluster"
Dans notre exemple nous avons 3 serveurs :
#### ubu_001 - Node1  
  | Domain  | Port  | Commentaire       |  
  |---------|------:|-------------------|  
  | Active  | 8080  | Domain controleur |  
  | Passive | 18081 |                   |  
#### slx001 - Node2  
  |  Domain | Port  | Commentaire       |  
  |---------|------:|-------------------|  
  | Active  |  8080 |                   |  
  | Passive | 18082 | Domain controleur |  
####  slx002 - Node3  
  |  Domain | Port  | Commentaire       |  
  |---------|------:|-------------------|  
  | Active  |  8080 |                   |  
  | Passive | 18083 |                   |  

![Schema : JBoss installation domain pour cluster][ShemaArchiServeurs]

### Configuration du Controleur de Domaine
Ici on modifie les fichiers sur ubu_001 (pour le domain active)
#### host.xml
Le controleur de domaine est aussi un controleur d'hote. Cependant il se configure de façon particulière. Un exemple est disponible dans le fichier `<JBOSS_DOMAIN_HOME>/domain/configuration/host-master.xml`.  
J'ai copié le fichier `<JBOSS_DOMAIN_HOME>/domain/configuration/host.xml` en `<JBOSS_DOMAIN_HOME>/domain/configuration/host-origine.xml` afin de ne pas modifier le fichier original.
Spécifier le nom du master :
```xml
<host name="node1" xmlns="urn:jboss:domain:4.1">
    [...]
</host>
```
Le noeud domain-controller doit pointer sur local :
```xml
<domain-controller>
    <local/>
</domain-controller>
```
Le noeud `interfaces/interface[@name="management"]` doit être configuré de sorte que les membre du domaine puisse y accèder :
```xml
<inet-address value="${jboss.bind.address.management:127.0.0.1}"/>
```
Les port de management doivent être configuré (ici valeur par defaut):
```xml
<management-interfaces>
    <native-interface security-realm="ManagementRealm">
        <socket interface="management" port="${jboss.management.native.port:9999}"/>
    </native-interface>
    <http-interface security-realm="ManagementRealm" http-upgrade-enabled="true">
        <socket interface="management" port="${jboss.management.http.port:9990}"/>
    </http-interface>
</management-interfaces>
```
On ajoute ensuite le serveur au domaine afin qu'il soit lui aussi hôte
```xml
<servers>
    <server name="node1-server" group="demo-cluster" auto-start="true">
        <system-properties>
            <property name="live-group" value="1"/>
            <property name="backup-group" value="2"/>
        </system-properties>
        <socket-bindings port-offset="0"/>
    </server>
</servers>
```

#### domain.xml
C'est dans ce fichier que l'on va paramètrer les sous-système du serveur.  
Editer le fichier `<JBOSS_DOMAIN_HOME>/domain/configuration/domain.xml`.  
Nous allons utiliser uniquement le profile `full-ha`, nous allons donc uniquement configurer les sous-système dans ce profile.  
Afin que le serveur partage les journeau JMS, il faut configurer le sous-système de activeQ  
> voir chapitre ActiveMQ Artemis  

Le noeud `<server-groups>` liste les groupes du domaine.  
Je définit le groupe suivant :  
```xml
<server-groups>
    <server-group name="demo-cluster" profile="full-ha">
        <jvm name="default">
            <heap size="66m" max-size="512m"/>
        </jvm>
        <socket-binding-group ref="full-ha-sockets"/>
    </server-group>
</server-groups>
```
#### démarrage du serveur
Une fois la configuration minimum faite nous pouvons démarrer le serveur avec la commande suivante :  
`<JBOSS_HOME>/bin/domain.sh -b 192.168.6.69 -bprivate=192.168.6.69 -Djboss.bind.address.management=192.168.6.69`
<!-- `<JBOSS_HOME>/bin/domain.sh --host-config=host-master.xml -Djboss.domain.base.dir=<JBOSS_HOME>/domain/` -->

Pour moi celà donne :
`/opt/jboss/jboss-eap-7.0-active/bin/domain.sh -b 192.168.6.69 -bprivate=192.168.6.69 -Djboss.bind.address.management=192.168.6.69`
<!-- `/opt/jboss/jboss-eap-7.0-active/bin/domain.sh --host-config=host.xml -Djboss.domain.base.dir=/opt/jboss/jboss-eap-7.0-active/domain/` -->

### Configuration du Controleur d'hote
Cette configuration est à effectué sur tout les serveur controleur d'hote qui n'est pas domaine controleur.

#### host-slave.xml
La configuration par defaut de ce fichier pointe déjà vers un controleur de domain, à condition de spécifier la variable jboss.domain.master.address

on nome l'hôte  :
```xml
<host name="node2" xmlns="urn:jboss:domain:4.1">
    [...]
</host>
```

Spécifier le nom du controleur d'hote
```xml
<host xmlns="urn:jboss:domain:4.0" name="jboss1-host">
    [...]
</host>
```
Il ne faut pas oublier de définir le username de connection :
```xml
<domain-controller>
    <remote security-realm="ManagementRealm" username="node2">
        <discovery-options>
            <static-discovery name="primary" protocol="${jboss.domain.master.protocol:remote}" host="${jboss.domain.master.address}" port="${jboss.domain.master.port:9999}"/>
        </discovery-options>
    </remote>
</domain-controller>
```
Il faut aussi ajouter l'authentification associé
```xml
    <management>
        <security-realms>
            <security-realm name="ManagementRealm">
                <server-identities>
                    <!-- Replace this with either a base64 password of your own, or use a vault with a vault expression -->
                        <secret value="cGFzc05vZGUyIQ==" />
                </server-identities>
                <authentication>
                    <local default-user="$local" skip-group-loading="true"/>
                    <properties path="mgmt-users.properties" relative-to="jboss.domain.config.dir"/>
                </authentication>
                <authorization map-groups-to-roles="false">
                    <properties path="mgmt-groups.properties" relative-to="jboss.domain.config.dir"/>
                </authorization>
            </security-realm>
        </security-realms>
        [...]
    </management>
```

Definition des groupes. Il faut définir l'offset du serveur si il y a plusieur serveur sur le m^me hôte :
```xml
<servers>
    <server name="node2-server" group="demo-cluster" auto-start="true">
        <system-properties>
            <property name="live-group" value="2"/>
            <property name="backup-group" value="3"/>
        </system-properties>
        <socket-bindings port-offset="0"/>
    </server>
</servers>
```

#### démarrage du server  
pour démarrer le serveur il faut spécifier l'adresse du domain controleur dans la variable jboss.domain.master.address:
`<JBOSS_HOME>/bin/domain.sh -b <ip_sereur> -bprivate=<ip_sereur> -Djboss.domain.master.address=<ip_sereur_DMC> --host-config=host-slave.xml`

Pour moi celà donne :
`/opt/jboss/jboss-eap-7.0-active/bin/domain.sh -b 192.168.6.92 -bprivate=192.168.6.92 -Djboss.domain.master.address=192.168.6.69 --host-config=host-slave.xml`

<!-- `<JBOSS_HOME>/bin/domain.sh --host-config=host-slave.xml -Djboss.domain.base.dir=<JBOSS_DOMAIN_HOME>/domain/ -Djboss.domain.master.address=192.168.6.69` -->  

-----------------------------------------------------------------------------------

# installaton cluster ou High Availability (HA)  
*(voir laptop-vroydor/C:/Users/vroydor/Documents/Documentation/JBoss/EAP 7/Red_Hat_JBoss_Enterprise_Application_Platform-7.0-Configuration_Guide-en-US.pdf §.2.2)*

Un cluster peut etre configurer de 2 façons  
* load Balancing  
> ce système est fait pour répartir la charge entre les noeuds du serveur. Utile si le nombre de données a transférer est important ou si le nombre de connection est grand.
* failover
> permet d'avoir toujours accès à l'application avec un partage des objets de session entre les noeud  

Le ha service ne démarre que si une application configuré comme distribuable est déployé

## HTTP Session  
*(voir laptop-vroydor/C:/Users/vroydor/Documents/Documentation/JBoss/EAP 7/Reference_Architectures-2017-Configuring_a_Red_Hat_JBoss_EAP_7_Cluster-en-US.pdf §.2.3)*

2 strategies sont disponibles:
* Replication  
> maintient une copie de la session sur chaque noeud. grande disponibilité mais couteux et lourd en mémoire
* Distribution  
> maintient une copie de la session sur certain noeud. Permet de spécifier le nombre de "owner" ou le nombre de copie de la session.

## Java Persistance
La session est géré a l'exterieur du serveur jboss. La plus part du temps par le SGBD.
Au niveau des caches :
* le cache de premier niveau n'est pas "clusterisé" car il est sensé avoir une durée de vie courte
* Utilisé le cache de second niveau créé une inconsistance des donnée car le flush des données en base ne peux être fais que sur le même noeud, ça introduit donc un aspect de statfull. Le chache de second niveau est géré par infinispan et fait une invalidation du cache. 

## ActiveMQ Artemis
*(voir laptop-vroydor/C:/Users/vroydor/Documents/Documentation/JBoss/EAP 7/Reference_Architectures-2017-Configuring_a_Red_Hat_JBoss_EAP_7_Cluster-en-US.pdf §.2.8)*

Dans un cluster il y a toujours qu'un seul qui répond, les autres sont en backup.
possède 2 mise en oeuvre:
* Data Replication
> Maintient une copie des message sur chaque server *implique un cout lors de l'utilisation normal*. Lors du démmarage le serveur se synchronize avec le maitre. Celà peut prendre du temps.
* Shared Store
> Nécessite un dossier partager entre les membres.  
> > Attention le dossier doit etre partager sur tout les membre, il ne doit pas etre sur un des membre puis partageé aux autres.
> > certaine option sont conseillé lors du montage sur le client :  
> > * sync : permet d'écrire immédiatement le buffer sur disque  
> > * intr : permet de gérer les interuption du serveur  
> > * noac : Désactive les attributs de caches. Nécessaire pour être cohérent entre les clients  
> > * soft : permet de reporter une erreur si le serveur n'est pas disponible  
> > * lookupcache=none : désactive la recherche de cache  
> 
> *Red hat ne recomande pas l'utilisation d'un NAS, mais privilégie l'utilisation d'un SAN*
> N'implique pas de coût lors de l'utilisation normal mais nécésite le chargement de tous les messages lors de la reprise par un backup.
  
Dans le cluster installé, j'utilise la deusième solution, `Shared Store`.

Le fichier domain.xml est donc adapté dans ce sens, la configuration se fait au niveau du noeud `subsystem[@xmlns=urn:jboss:domain:messaging-activemq*]`
Pour la configuration par defaut du serveur activeMQ
Il faut préciser les informations d'un utilisateur ayant les droits d'administration sur le serveur
```xml
<cluster password="passAdmn!" user="admin" />
```
Afin de préciser que l'on utilise un dossier partagé, mon chemin est `/mnt/hgfs/public/activeMQ/sharefs`, il faut définir
```xml
<shared-store-master failover-on-server-shutdown="true"/>
<bindings-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/bindings"/>
<journal-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/journal"/>
<large-messages-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/largemessages"/>
<paging-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/paging"/>
```
On ajoute la possibilité au autres serveur de se connecter depuis l'exterieur
```xml
<remote-connector name="netty" socket-binding="messaging">
    <param name="use-nio" value="true"/>
    <param name="use-nio-global-worker-pool" value="true"/>
</remote-connector>
```
on ajoute aussi l'acceptation asocié
```xml
<remote-acceptor name="netty" socket-binding="messaging">
    <param name="use-nio" value="true"/>
</remote-acceptor>
```
Il faut pas oublier d'adapter la remote connection factory avec le connecteur netty que l'on vient d'ajouter
```xml
<connection-factory name="RemoteConnectionFactory" reconnect-attempts="-1" block-on-acknowledge="true" ha="true" entries="java:jboss/exported/jms/RemoteConnectionFactory" connectors="netty"/>
```

J'ajoute aussi la configuration du backup pour un autre system ActiveQ
```xml
<server name="backup">
    <cluster password="passAdmin!" user="admin"/>
    <shared-store-slave failover-on-server-shutdown="true"/>
    <bindings-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/bindings"/>
    <journal-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/journal"/>
    <large-messages-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/largemessages"/>
    <paging-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/paging"/>
    <address-setting name="#" redistribution-delay="1000"/>
    <remote-connector name="netty-backup" socket-binding="messaging-backup"/>
    <remote-acceptor name="netty-backup" socket-binding="messaging-backup"/>
    <broadcast-group name="bg-group-backup" connectors="netty-backup" broadcast-period="1000" jgroups-channel="activemq-cluster"/>
    <discovery-group name="dg-group-backup" refresh-timeout="1000" jgroups-channel="activemq-cluster"/>
    <cluster-connection name="amq-cluster" discovery-group="dg-group-backup" connector-name="netty-backup" address="jms"/>
</server>
```
Ce qui me donne le sous-system suivant
```xml
<subsystem xmlns="urn:jboss:domain:messaging-activemq:1.0">
    <server name="default">
        <cluster password="passAdmin!" user="admin"/>
        <shared-store-master failover-on-server-shutdown="true"/>
        <bindings-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/bindings"/>
        <journal-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/journal"/>
        <large-messages-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/largemessages"/>
        <paging-directory path="/mnt/nfs/activeMQ/sharefs/group-${live-group}/paging"/>
        <security-setting name="#">
            <role name="guest" delete-non-durable-queue="true" create-non-durable-queue="true" consume="true" send="true"/>
        </security-setting>
        <address-setting name="#" redistribution-delay="1000" message-counter-history-day-limit="10" page-size-bytes="2097152" max-size-bytes="10485760" expiry-address="jms.queue.ExpiryQueue" dead-letter-address="jms.queue.DLQ"/>
        <http-connector name="http-connector" endpoint="http-acceptor" socket-binding="http"/>
        <http-connector name="http-connector-throughput" endpoint="http-acceptor-throughput" socket-binding="http">
            <param name="batch-delay" value="50"/>
        </http-connector>
        <remote-connector name="netty" socket-binding="messaging">
            <param name="use-nio" value="true"/>
            <param name="use-nio-global-worker-pool" value="true"/>
        </remote-connector>
        <in-vm-connector name="in-vm" server-id="0"/>
        <http-acceptor name="http-acceptor" http-listener="default"/>
        <http-acceptor name="http-acceptor-throughput" http-listener="default">
            <param name="batch-delay" value="50"/>
            <param name="direct-deliver" value="false"/>
        </http-acceptor>
        <remote-acceptor name="netty" socket-binding="messaging">
            <param name="use-nio" value="true"/>
        </remote-acceptor>
        <in-vm-acceptor name="in-vm" server-id="0"/>
        <broadcast-group name="bg-group1" connectors="netty" jgroups-channel="activemq-cluster"/>
        <discovery-group name="dg-group1" jgroups-channel="activemq-cluster"/>
        <cluster-connection name="amq-cluster" discovery-group="dg-group1" connector-name="netty" address="jms"/>
        <jms-queue name="ExpiryQueue" entries="java:/jms/queue/ExpiryQueue"/>
        <jms-queue name="DLQ" entries="java:/jms/queue/DLQ"/>
        <jms-queue name="DistributedQueue" entries="java:/queue/DistributedQueue"/>
        <connection-factory name="InVmConnectionFactory" entries="java:/ConnectionFactory" connectors="in-vm"/>
        <connection-factory name="RemoteConnectionFactory" reconnect-attempts="-1" block-on-acknowledge="true" ha="true" entries="java:jboss/exported/jms/RemoteConnectionFactory" connectors="netty"/>
        <pooled-connection-factory name="activemq-ra" transaction="xa" entries="java:/JmsXA java:jboss/DefaultJMSConnectionFactory" connectors="in-vm"/>
    </server>
    <server name="backup">
        <cluster password="passAdmin!" user="admin"/>
        <shared-store-slave failover-on-server-shutdown="true"/>
        <bindings-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/bindings"/>
        <journal-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/journal"/>
        <large-messages-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/largemessages"/>
        <paging-directory path="/mnt/nfs/activeMQ/sharefs/group-${backup-group}/paging"/>
        <address-setting name="#" redistribution-delay="1000"/>
        <remote-connector name="netty-backup" socket-binding="messaging-backup"/>
        <remote-acceptor name="netty-backup" socket-binding="messaging-backup"/>
        <broadcast-group name="bg-group-backup" connectors="netty-backup" broadcast-period="1000" jgroups-channel="activemq-cluster"/>
        <discovery-group name="dg-group-backup" refresh-timeout="1000" jgroups-channel="activemq-cluster"/>
        <cluster-connection name="amq-cluster" discovery-group="dg-group-backup" connector-name="netty-backup" address="jms"/>
    </server>
</subsystem>
```
Il faut aussi ajouter les socket binding  nommé `messaging` et `messaging-backup` utilisé par les connecteurs netty
Ces éléments s'ajoute dans le noeud `/domain/socket-binding-groups/full-ha-sockets`
```xml
<socket-binding name="messaging" port="5545"/>
<socket-binding name="messaging-backup" port="5546"/>
``` 
## HTTP Connectors
*(voir laptop-vroydor/C:/Users/vroydor/Documents/Documentation/JBoss/EAP 7/Reference_Architectures-2017-Configuring_a_Red_Hat_JBoss_EAP_7_Cluster-en-US.pdf §.2.9)*  

JBoss EAP 7 est compatible avec plusieurs connecteurs HTTP.
* mod_cluster
> type load balancer, disponible par défault, permet la découverte de proxy via multicast.  
  la charge est estimé par le noeud lui même. Il prend en compte la charge CPU, Heap, ...  

