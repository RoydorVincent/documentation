[Bridge_documentation_source]: http://www.mastertheboss.com/howto/jboss-jms6/configuring-jms-bridge-with-wildfly-10
[remoteJMSDestination_Documentation_Source]: http://www.mastertheboss.com/jboss-server/jboss-jms/connecting-to-an-external-wildfly-jms-server

#JMS
# Utilisation des EJB 3.2
## Utilisation en local
### Définition dans le MDB
l'annotation `@JMSDestinationDefinition` permet de créer la destination.
L'annotation `@MessageDriven` permet de définir la queue ou le topic à utiliser. Grâce à cette annotation, il est possible de définir la destination en question.  
Voici un exemple pour une queue :  

```java
@JMSDestinationDefinition(name = "java:jboss/exported/jms/queue/HELLOWORLDMDBQueue",
                 interfaceName = "javax.jms.Queue",
                 destinationName = "HelloWorldQueueMDB")
@MessageDriven(name = "HelloWorldQueueMDB", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/HELLOWORLDMDBQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
class HelloWorldQueueMDB implements MessageListener {
    
    public void onMessage(Message rcvMessage) {
        // TODO manage message
    }
         
}
```
exemple de l'injection de la queue :
```java
@Resource(lookup = "java:/queue/HELLOWORLDMDBQueue")
private Queue queue;
```

### Définition standalone.xml / cli
Il est aussi possible de le définir dans le `standalone.xml` :

```xml
<jms-queue name="HelloWorldQueueMDB" entries="queue/HELLOWORLDMDBQueue java:jboss/exported/jms/queue/HELLOWORLDMDBQueue"/>
```

ou encore on peut l'ajouter via une commande cli :  

```
jms-queue add --queue-address=HelloWorldQueueMDB --entries=queue/HELLOWORLDMDBQueue java:jboss/exported/jms/queue/HELLOWORLDMDBQueue
```

## Connection à une queue en remote
### Définition dans le MDB
L'annotation `@MessageDriven` permet de définir la queue ou le topic a utiliser. Grâce à cette annotation, il est possible de définir la destination en question.  
Voici un exemple pour une queue :  

```java
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/HelloWorldRemoteQueueMDB"),  
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),  
    @ActivationConfigProperty(propertyName = "user", propertyValue = "user123"),  
    @ActivationConfigProperty(propertyName = "password", propertyValue = "Password123"),  
    @ActivationConfigProperty(propertyName = "connectionParameters", propertyValue = "host=remotehost;port=5445"),  
    @ActivationConfigProperty(propertyName = "connectorClassName", propertyValue = "org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory")  
    }, mappedName = "HelloWorldRemoteQueueMDB")
class HelloWorldRemoteQueueMDB implements MessageListener {
    
     public void onMessage(Message rcvMessage) {
         // TODO manage message
     }
    
}
```

exemple de l'injection de la queue :
```java
@Inject
private JMSContext context;

@Resource(lookup = "java:/queue/HelloWorldRemoteQueueMDB")
private Queue queue;
```

### Définition standalone.xml / cli
ou alors en gardant l'annotation identique quelque soit l'état de la queue (local ou remote), il faut fair cette configuration sur le serveur.  
dnas le `standalone.xml`  

création d'un remote connector, de la queue et de la factory
```xml
<subsystem xmlns="urn:jboss:domain:messaging-activemq:1.0">
    <server name="default">
        [...]
        <remote-connector name="remote-queue-connector" socket-binding="remote-queue-socket"/>
        [...]    
        <jms-queue name="HelloWorldRemoteQueueMDB" entries="queue/HelloWorldRemoteQueueMDB java:jboss/exported/jms/queue/HelloWorldRemoteQueueMDB"/>
        [...]
        <pooled-connection-factory name="remote-conn-factory" entries="java:/jms/remoteCF java:jboss/exported/jms/remoteCF" connectors="remote-queue-connector" user="user123" password="Password123" />
    </server>
</subsystem>
```

création du socket-binding
```xml
 <socket-binding-group name="standard-sockets" default-interface="public" port-offset="${jboss.socket.binding.port-offset:0}">
    [...]
    <outbound-socket-binding name="remote-queue-socket">
        <remote-destination host="remotehost" port="5445"/>
    </outbound-socket-binding>
 </socket-binding-group>
```

exemple de définition :
```java
@MessageDriven(name = "HelloWorldRemoteQueueMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/HelloWorldRemoteQueueMDB"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
class HelloWorldRemoteQueueMDB implements MessageListener {
    
     public void onMessage(Message rcvMessage) {
         // TODO manage message
     }
    
}
```

exemple de l'injection :
```java
@Inject
@JMSConnectionFactory("jms/remoteCF")
private JMSContext context;

@Resource(lookup = "java:/queue/HelloWorldRemoteQueueMDB")
private Queue queue;
```

### Définition d'un bridge
Un bridge permet de faire transiter des messages entre 2 destinations.
![ShemaArchiServeurs](resources/jms/jmsbridge.png)

#### Source
Il faut ajouter au fichier `standalone.xml` le définition du bridge
```xml
<server xmlns="urn:jboss:domain:4.1">
    <subsystem xmlns="urn:jboss:domain:messaging-activemq:1.0">
        <server name="default">
            [...]
            <http-connector name="bridge-connector" endpoint="bridge-acceptor" socket-binding="messaging-remote"/>
            [...]
            <jms-queue name="JMSBridgeSourceQueue" entries="queue/JMSBridgeSourceQueue java:jboss/exported/jms/queues/JMSBridgeSourceQueue"/>
            [...]
            <connection-factory name="RemoteConnectionFactory" entries="java:jboss/exported/jms/RemoteConnectionFactory" connectors="bridge-connector"/>
            [...]
        </server>
        <jms-bridge name="simple-jms-bridge" max-batch-time="100" max-batch-size="10" max-retries="1" failure-retry-interval="10000" quality-of-service="AT_MOST_ONCE">
            <source destination="queue/JMSBridgeSourceQueue" connection-factory="ConnectionFactory"/>
            <target password="guest" user="guest" destination="jms/queues/JMSBridgeTargetQueue" connection-factory="jms/RemoteConnectionFactory">
                <target-context>
                    <property name="java.naming.factory.initial" value="org.jboss.naming.remote.client.InitialContextFactory"/>
                    <property name="java.naming.provider.url" value="http-remoting://localhost:8180"/>
                </target-context>
            </target>
        </jms-bridge>
    </subsystem>
    
    <socket-binding-group name="standard-sockets" default-interface="public" port-offset="${jboss.socket.binding.port-offset:0}">
        [...]
        <outbound-socket-binding name="messaging-remote">
            <remote-destination host="localhost" port="8180"/>
        </outbound-socket-binding>
    </socket-binding-group>

</server>
```

#### target
Il faut ajouter la target queue
```xml
<server xmlns="urn:jboss:domain:4.1">
    [...]
    <subsystem xmlns="urn:jboss:domain:messaging-activemq:1.0">
        <server name="default">
            [...]
            <jms-queue name="JMSBridgeTargetQueue" entries="queue/JMSBridgeTargetQueue java:jboss/exported/jms/queues/JMSBridgeTargetQueue"/>
            [...]
        </server>
    </subsystem>
    [...]
</server>
```

