[git-config-documentation]: https://gitirc.eu/git-config.html "Documentation de git config"


# Configuration de GIT
une documentation est disponible pour la commande git config sous :  
> [git config][git-config-documentation]  

## Sensibilité à la case des nom de fichier
>`git config core.ignorecase false`  
