# Fusionner une branche sur master
Je cherche a merger ma feature sous forme d'un seul commit sur le master.  
Voici mon état actuel.  
![new feature](resources/featureToMerge.png)

La branche actuelement active est newfeature et tous les changement ont été poussé sur le dépôt distant.  
Il faut que j'active la branche master.  
`$ git checkout master`  
2 possibilités sont possibles,  
* merge `git merge newfeature`  
    ![merge a feature](resources/resultWithMerge.png)  
* rebase `git rebase newfeature`  
    ![rebase a feature](resources/resultWithRebase.png)

# Bibliographies
http://gitready.com/intermediate/2009/01/31/intro-to-rebase.html
https://git-scm.com/book/fr/v1/Les-branches-avec-Git-Brancher-et-fusionner%C2%A0%3A-les-bases