# README #
Habituellement dans cette section on retrouve comment compiler et lancer l'application. Mais mon projet ici n'est pas une application.  

_[Utilisation de Markdown](https://bitbucket.org/tutorials/markdowndemo)_  


Ce dépôt regroupe les connaissances sur différents éléments.  

* Apache - Tomcat
    * [Installation de apache 2](/Apache/Installation%20apache2.md)  
    * [Utilisation Tomcat](/Apache/UtilisationTomcat.md)  
* L'architecture applicative  
    * [DDD](/ArchitectureAplicative/DDD.md)  
    * [CQRS](/ArchitectureAplicative/CQRS.md)  
* GIT  
    * [Commendes GIT](/GIT/GIT_command.md)
    * [Configuration de GIT](/GIT/GIT_config.md)  
* Javascript  
    * [Premier pas](/Javascript/javascript_premierPas.md)  
* JBoss  
    * [Configuration de JBoss](/JBoss/JBoss_Config.md)  
    * [JMS](/JBoss/JMS.md)  
    * [Securite JBoss](/JBoss/Security.md)  
* Liquibase  
    * [Mise en oeuvre](/liquibase/LiquidBase%20-%20Mise%20en%20Oeuvre.md)  
* Méthodologie  
    * [Behavior Driven Development](/Methodologie/BDD.md)  
* RestEasy  
    * [Nouveau projet](/RestEasy/new%20projet%20avec%20RestEasy.md)  
* Sonar  
    * [Integration avec Maven](/Sonar/Sonar.md)  
* Generation Documentation
    * [Web Service avec Enunciate](/GenerationDocumentation/Enunciate.md)
    
   