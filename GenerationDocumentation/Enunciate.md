# Enunciate

## Configuration
Enunciate se configure via un fichier xml nommé `enunciate.xml` a positionner à la racine du projet (même niveau que le pom)

Voici un exemple de ce fichier :
```xml
<?xml version="1.0" encoding="UTF-8"?>
<enunciate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="http://enunciate.webcohesion.com/schemas/enunciate-2.7.0.xsd">
	
	<!-- Si la balise title n'existe pas ou est commentée, Enunciate utilise par défaut le nom du projet Maven -->
    <!-- <title></title> -->
    <!-- La description est l'en-tête du site de documentation. Vous pouvez aussi la fournir dans un fichier html séparé : <description file="enunciate-description.part.html"/> -->
    <description>
	    <![CDATA[
			<h1>Ca c'est la description</h1>
			<p>Ici peut être afficher la description du projet</p>
		]]>
	</description>
	<copyright><![CDATA[Copyright (c) Roydor<br/>]]></copyright>
	<!-- Autres balises disponibles : <terms> <code-license> <api-license> <contact> <application> <facets> <styles> <namespaces> : voir https://github.com/stoicflame/enunciate/wiki/User-Guide -->
    
	
	<api-classes>
		<include pattern="my.project.package.**" />
		<include pattern="my.other.project.package.**" />
		<!-- Exclusion des classes de l'implémentation de jax-rs, ici resteasy -->
		<exclude pattern="org.jboss.resteasy.core.**" />
	</api-classes>
	<modules>
		<!-- Support des annotations jax-rs avec Jackson. EAP6 nous fournit Jackson 1.9.11 -->
		<!-- Si certains types ou endpoints ne sont pas trouvés, essayez d'ajouter l'attribut datatype-detection="aggressive" aux modules ci-dessous -->
		<jaxrs />
		<jackson1>
            <!-- Si vous utilisez des mixins Jackson pour configurer la sérialisation de certaines classes, il faut les déclarer ici -->
            <!-- <mixin source="MyMixin" target="MyClass"> -->
        </jackson1>
        <!-- <jackson /> --><!-- décommenter pour utiliser Jackson2 -->
		<jackson  />
		<!-- Génération du swagger.json -->
		<swagger basePath="/${project.name}/rest" />
		<!-- Support de jax-ws : activez-le pour documenter en même temps vos services SOAP ! -->
		<jaxws />
		<!-- Vous pouvez personnaliser le style et beaucoup d'autres choses dans la documentation générée.
             Voir la documentation : https://github.com/stoicflame/enunciate/wiki/Module-Docs -->
		<docs  apiRelativePath="../${project.name}/" />
		<!-- Génération des clients. Désactivée ici. -->
		<java-json-client disabled="true"/>
		<java-xml-client disabled="true"/>
		<javascript-client disabled="true"/>
		<c-xml-client disabled="true"/>
		<csharp-xml-client disabled="true"/>
		<gwt-json-overlay disabled="true"/>
		<obj-c-xml-client disabled="true"/>
		<php-json-client disabled="true"/>
		<php-xml-client disabled="true"/>
		<ruby-json-client disabled="true"/>
		<!-- A moins que vous n'utilisiez Spring, vous n'avez pas besoin de ce module -->
		<spring-web disabled="true"/>
	</modules>
</enunciate>
```

## Profile maven
```xml
<profiles>
	<profile>
		<id>DocRest</id>
		<build>
			<plugins>
				<plugin>
					<groupId>com.webcohesion.enunciate</groupId>
					<artifactId>enunciate-maven-plugin</artifactId>
					<version>2.7.1</version>
					<configuration>
						<enunciateArtifactId>${project.artifactId}Enunciate</enunciateArtifactId>
						<sourcepath-includes>
							<!-- Ajoutez autant de sourcepath-include que nécessaire. L'artifactId est facultatif (il prend alors tous les artefacts avec ce groupId trouvés dans les dépendances maven).
								 Le groupId du projet maven est ajouté par défaut -->
							<sourcepath-include>
								<groupId>my.dependency.library.group</groupId>
							</sourcepath-include>
							<sourcepath-include>
								<groupId>my.other.project.group</groupId>
							</sourcepath-include>
						</sourcepath-includes>
					</configuration>
					<executions>
						<execution>
							<id>docs</id>
							<goals>
								<goal>docs</goal>
							</goals>
							<configuration>
								<!-- the directory where to put the docs -->
								<docsDir>${build.directory}/docs</docsDir>
								<!--<docsDir>${build.directory}/${build.finalName}/docs</docsDir>-->
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<artifactId>maven-assembly-plugin</artifactId>
					<configuration>
						<descriptors>
							<descriptor>src/main/assembly/apidocs.xml</descriptor>
						</descriptors>
					</configuration>
					<executions>
						<execution>
							<phase>package</phase>
							<goals>
								<goal>single</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</build>
	</profile>
</profiles>
```

Normalement l'assemblage vas chercher le fichier `apidocs.xml` qui se situe dans `src/main/assembly` voici un exemple de ce a quoi il devrait resembler:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<assembly xmlns="http://maven.apache.org/ASSEMBLY/2.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/ASSEMBLY/2.0.0 http://maven.apache.org/xsd/assembly-2.0.0.xsd">
    <id>apidocs</id>
    <formats>
        <format>war</format>
    </formats>
    <includeBaseDirectory>false</includeBaseDirectory>
    <fileSets>
        <fileSet>
            <outputDirectory>.</outputDirectory>
            <directory>target/docs/apidocs/</directory>
        </fileSet>
    </fileSets>
    <files>
        <file>
            <source>src/main/assembly/apidocs.web.xml</source>
            <outputDirectory>WEB-INF</outputDirectory>
            <destName>web.xml</destName>
            <lineEnding>unix</lineEnding>
        </file>
        <file>
            <source>src/main/assembly/apidocs.jboss-web.xml</source>
            <outputDirectory>WEB-INF</outputDirectory>
            <destName>jboss-web.xml</destName>
            <lineEnding>unix</lineEnding>
            <filtered>true</filtered>
        </file>
    </files>
</assembly>
```

ici on fait reference a d'autre fichier que voici les exemples:
`apidocs.jboss-web.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<jboss-web>
    <context-root>${project.artifactId}-apidocs</context-root>
</jboss-web>
```
ainssi que `apidocs.web.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
        version="3.0">
    <display-name>Ma Documentation API documentation</display-name>
    <distributable />
</web-app>
```