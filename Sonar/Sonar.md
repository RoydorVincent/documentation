#Integration de Sonar dans un projet maven
## SonarCloud
Dans le fichier settings.xml de maven je rajoute les properties nécessaire aà la connection de Sonar
```xml
<settings>
    [...]
    <pluginGroups>
        <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
    </pluginGroups>
    [...]
    <profiles>
        <profile>
            <id>sonarCloud</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <properties>
                <sonar.host.url>https://sonarcloud.io</sonar.host.url>
                <sonar.organization>user-bitbucket</sonar.organization>
                <sonar.login>**Token**</sonar.login>
            </properties>
        </profile>
    </profiles>
</settings>
```
`<sonar.organization>user-bitbucket</sonar.organization>` peut être créée ici : [https://sonarcloud.io/account/organizations](https://sonarcloud.io/account/organizations)  
`<sonar.login>**Token**</sonar.login>` peut être généré ici : [https://sonarcloud.io/account/security/](https://sonarcloud.io/account/security/)  