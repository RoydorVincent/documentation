#Angular  

##Pour désinstallé Angular CLI  
'''bash
npm uninstall -g @angular/cli
'''

#Commencer aver angular/cli  
Tout d'abord il faut insatller angular/cli. Executer la commande suivante :  
'''bash
npm install -g @angular/cli
'''
Afin de verifier de la version de angular/cli installé, il est possible d'executer la commande suivante  
'''bash
ng version
'''

##Mettre à jour Node.js
Afin de mettre a jour ynode, on peut utiliser la commande npm suivante
'''bash
npm update -g
'''
Cette commande ne marche pas sur un windows, il faut donc voir : https://nodejs.org/en/download/current/

##Créer un projet
il est possible de créer unprojet grace à la commande :  
'''bash
ng new <NomProjet> --prefix <Prefix> --defaults
'''

##Spécifier les dépendances
Afin de gérer les dépendances, il faut les définir dans le fichier "package.json"  

##Mettre à jour le projet
Afin de mettre à jour les dépendances d'un projet suite a un ajout ou suite a la création d'un nouveau projet, il faut se placer dans ce projet puis executer  
'''bash
npm install
'''

Install Angular Material
[https://material.angular.io/guide/getting-started]
ng add @angular/material