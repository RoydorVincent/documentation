[yarnSite]: https://yarnpkg.com/ "Site officiel de yarn"  
[yarnInstallSite]: https://yarnpkg.com/fr/docs/install "info instal yarn"
[projetGit_SONAR]: https://github.com/SonarSource/SonarTS "projet git de SonarTS plug-in lint pour SonarQube"
<!-- UI Libraries -->
[angularMaterial_Site]: https://material.angular.io "site du framework CSS Angular Material"
[angularMaterial_HowToUse]: https://material.angular.io/guide/getting-started "Get Started du site Angular Material"
[angularMaterial_api]: https://material.angularjs.org/latest/api "api de framwork Angular Material"
[ngBootstrap_Site]: https://ng-bootstrap.github.io "sit du framework Bootstrap"
# Javscript Mes Premiers pas
L'utilisation de typescript est utilisé afin de typer le langage.  
L'avantage est que l'écriture se fait dans un fichier portant l'extension ".ts"  
Lors de la compilation se fichier génère un fichier javascript ainsi un fichier map permettant la retro compatibilité (écriture de javscript respectant la norme es6 avec retro compatibilité vers es5).  
# Angular  
S'utilise sous forme de composant. un composant est constitué par :  
 * vue : fichier HTML appelé template  
 * model/contrôleur : fichier ts  
 * peux avoir une feuille css associé

Bonne pratique un composant est dans un dossier, les fichier sont suffixé par ".component"  
Exemple un composant nommé races contiendra les fichiers suivant:  
 * races.component.html
 * races.component.ts
 * reces.component.css
 
# Première install
 * installer nodejs  
 * installer angular cli  
    > `npm install -g @angular/cli`  
 * installer yarn : [yarnInstallSite][install yarn]  
 * Spécifier a angularCli d'utilisé yarn comme packageManager
     >`ng set --global packageManager=yarn`  
     
# Création projet
Permet de créer le squelette de projet.
>`ng new <nomProjet> --prefix <préfix>`  

## Ajout framework CSS
### ng-bootstrap
Installation de bootstrap se lon le [site][ngBootstrap_Site]  
tout d'abord il faut installer les dépendances :   
> `npm install --save bootstrap`

Selon la formation nijaSquad, permet de géré la partie CSS. il faut ajouter boostrap au projet  
> `yarn add --exact bootstrap font-awesome`  

pui on peut installer le framwork:  
> `npm install --save @ng-bootstrap/ng-bootstrap`


ensuite on configure le projet pour l'utiliser en modifiant l'attribut `styles` dans le fichier `.angular-cli.json`  
```
"styles": [
     "../node_modules/bootstrap/dist/css/bootstrap.min.css",
     "../node_modules/font-awesome/css/font-awesome.min.css",
     "styles.css"
   ]
```  
### Angular Material
Framework géré par l'équipe de Angular [site web][angularMaterial_Site]
> `npm install --save @angular/material @angular/cdk`  
> `npm install --save @angular/animations`  

Voir [how to use][angularMaterial_HowToUse] sur le  site.
L'api des modules est disponible [ici][angularMaterial_api]
## Adapter la couverture de test Karma
Modifier l'attribut `test` dans le fichier `.angular-cli.json`  
```
"test": {
         "karma": {
           "config": "./karma.conf.js"
         },
         "codeCoverage": {
           "exclude": [
             "src/app/models/*.ts",
             "src/app/app.module.ts"
           ]
         }
   }
```
La configuration des tests se fait dans le fichier karma.conf.js au niveau du noeud `coverageIstanbulReporter`
Le rapport de la couverture de test peut être généré dans différent format :
* `html` produces a bunch of HTML files with annotated source code.
* `lcovonly` produit un fichier lcov.info. Ce fichier est utile pour intelliJ
* `lcov` produit les fichiers HTML + .lcov. Format par défaut.
* `cobertura` produces a cobertura-coverage.xml file for easy Hudson integration.
* `text-summary` produces a compact text summary of coverage, typically to the console.
* `text` produces a detailed text table with coverage for all files.
Exemple :
```
coverageIstanbulReporter: {
      reports: [ 'html', 'json-summary', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
```

### Erreur lors de l'exécution des tests  
> L'erreur suivante peut se produire :  
> ```
> Server start failed on port 9876: Error: No provider for "framework:@angular-devkit/build-angular"!
> ```
> La configuration de karma demande d'utilisé le framework '@angular-devkit/build-angular'.  
> La solution est d'ajouter le framework dans les dépendances de dev a l'aide de la commande :  
> `npm install @angular-devkit/build-angular --save-dev`  

> Un autre comportement peut indiquer : 
> ```
> INFO [karma]: Karma v0.9.2 server started at http://localhost:9876/
> WARN [launcher]: Launcher "phantomjs" is not registered!
> ```
> Dans ce cas il faut verifier que le fichier de configuration de karma contienne bien :  
> `plugins: ['karma-jasmine', 'karma-phantomjs-launcher']`

# Commande NG
## Créer un projet
> `ng new <nomProjet> --prefix <préfix>` 
## Ajouter un composant
> `ng generate component <nom_component_CapleCase>`
## Ajouter un service
> `ng generate service <nom_service_CapleCase`
## Verifier la qualité du code via tslint
> `ng lint --type-check`
## Exécution des tests une seule fois avec le code coverage
> `ng test --single-run --code-coverage`

# Commandes NPM
## Changer la version dans package.json
npm install -g change-version


# Librairies
## gestion des websocket
Ce protocole permet au serveur de broadcaster, sur un modèle de publication/souscription, des événements aux clients connectés via WebSocket. Ce modèle est très pratique.  
> `yarn add --exact webstomp-client`

# integration avec SONAR
 voir [plug-in lint][projetGit_SONAR]