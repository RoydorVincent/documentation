# New Projet avec RestEasy
[DocResteasy]: https://docs.jboss.org/resteasy/docs/3.1.4.Final/userguide/html/index.html "Documentation de rest easy 3.1.4.Final"

## Pour TOMCAT
Ici je prépare les dépendances pour Tomcat :

### Servlet API
```xml
<properties>
    <javax.servlet-api.version>3.1.0</javax.servlet-api.version>
</properties>
```
```xml
<dependencies>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>${javax.servlet-api.version}</version>
    </dependency>
</dependencies>
```
### Resteasy
```xml
<properties>
    <resteasy.version>3.1.4.Final</resteasy.version>
</properties>
```
```xml
<dependencies>
    <!-- core library -->
    <dependency>
        <groupId>org.jboss.resteasy</groupId>
        <artifactId>resteasy-jaxrs</artifactId>
        <version>${resteasy.version}</version>
    </dependency>

    <dependency>
        <groupId>org.jboss.resteasy</groupId>
        <artifactId>resteasy-jackson2-provider</artifactId>
        <version>${resteasy.version}</version>
    </dependency>


    <!-- Integration interface in Servlet 3.0 containers to initialize an application -->
    <dependency>
        <groupId>org.jboss.resteasy</groupId>
        <artifactId>resteasy-servlet-initializer</artifactId>
        <version>${resteasy.version}</version>
    </dependency>
    
    <!-- integration with CDI -->
    <dependency>
        <groupId>org.jboss.resteasy</groupId>
        <artifactId>resteasy-cdi</artifactId>
        <version>${resteasy.version}</version>
    </dependency>
</dependencies>
```