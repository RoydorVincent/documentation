REM script de génération des fichier HTML

rmdir /S /Q target

REM Apache folder
SET RepName=Apache
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\InstallationApache2.html %RepName%\InstallationApache2.md
pandoc -f markdown -t html -o target\%RepName%\UtilisationTomcat.html %RepName%\UtilisationTomcat.md

REM ArchitectureAplicative folder
SET RepName=ArchitectureAplicative
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\CQRS.html %RepName%\CQRS.md
pandoc -f markdown -t html -o target\%RepName%\DDD.html %RepName%\DDD.md
pandoc -f markdown -t html -o target\%RepName%\EventSourcing.html %RepName%\EventSourcing.md

REM GenerationDocumentation
SET RepName=GenerationDocumentation
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\CQRS.html %RepName%\CQRS.md
pandoc -f markdown -t html -o target\%RepName%\DDD.html %RepName%\DDD.md
pandoc -f markdown -t html -o target\%RepName%\EventSourcing.html %RepName%\EventSourcing.md

REM GIT folder
SET RepName=GIT
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\GIT_command.html %RepName%\GIT_command.md
pandoc -f markdown -t html -o target\%RepName%\GIT_config.html %RepName%\GIT_config.md

REM Javascript folder
SET RepName=Javascript
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
REM pas de dossier resources : xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\javascript_premierPas.html %RepName%\javascript_premierPas.md
pandoc -f markdown -t html -o target\%RepName%\useAngularProjectInMaven.html %RepName%\useAngularProjectInMaven.md

REM JBoss folder
SET RepName=JBoss
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\DataSource.html %RepName%\DataSource.md
pandoc -f markdown -t html -o target\%RepName%\JBoss_Config.html %RepName%\JBoss_Config.md
pandoc -f markdown -t html -o target\%RepName%\JMS.html %RepName%\JMS.md
pandoc -f markdown -t html -o target\%RepName%\Security.html %RepName%\Security.md

REM Liquibase
SET RepName=liquibase
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
REM pas de dossier ressources : xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\LiquidBase-Mise_en_Oeuvre.html "%RepName%\LiquidBase - Mise en Oeuvre.md"


REM Methodologie
SET RepName=Methodologie
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
REM pas de dossier ressources : xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\BDD.html "%RepName%\BDD.md"

REM RestEasy
SET RepName=RestEasy
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
REM pas de dossier ressources : xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\New_Project_avec_RestEasy.html "%RepName%\new projet avec RestEasy.md"

REM Sonar
SET RepName=Sonar
IF NOT exist "target\%RepName%\*.*" MKDIR target\%RepName%
REM copy resources folder
REM pas de dossier ressources : xcopy %RepName%\resources\*.* target\%RepName%\resources\*.* /S /R /Y

pandoc -f markdown -t html -o target\%RepName%\Sonar.html "%RepName%\Sonar.md"