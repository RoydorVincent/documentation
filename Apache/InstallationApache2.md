[modCluster_download]: http://mod-cluster.jboss.org/downloads "Download mod_cluster"

[mod_cluster_git]: https://github.com/modcluster/mod_cluster.git
[mod_cluster_tag_1.3.7Final]: https://github.com/modcluster/mod_cluster/tree/1.3.7.Final

# Installation de Apache 2 - Ubuntu  
Comme façade j'utilise le serveur httpd apache pour rediriger les requêtes sur le cluster de serveurs d'application.  
L'installation se fait sur Linux (Ubuntu).  
L'inconvénient c'est que le module `mod_cluster` n'est pas disponible de base.
Cependant il est disponible sur le sit de jboss sous [Download mod_cluster][modCluster_download] mais ne convient pas à nôtre utilisation.  
Nous allons donc le configurer pour nos besoins et le compiler.
## Avant de commencer
Mettre à jour les librairies système.  
```bash
apt-get update
```

## Installation de apache2
apache2 est dans le repo de ubuntu il suffit donc de l'installer depuis le repo.  
```bash
apt-get install -y apache2
```

## Configuration et build du module `mod_cluster`
apache2 est installé, on peux maintenant compiler le module. pour utilisé le module il faut construire en réalité 4 module apache :  
* mod_advertise
* mod_manager
* mod_proxy_cluster
* mod_cluster_slotmem
Apache2 utilise des fichiers pour loader les module. Nous ajouterons le fichier proxy_cluster.load dans les modules disponible.  
> Afin de builder un module il faut se placer dans son dossier.  
> Puis créer la configugartion,  
> executer la configuration désiré. Ici on ajoute la dépendance sur apxs.  
> Et enfin créer la librairie.  
> Pour finir on copie la librairie créé dans le dossier modules de apache
### Avant de commencer
Il faut certain outils afin de pouvoir récupèrer les source du module et de les compiler.  
Nous allons directement prendre une version final qui porte le tag `1.3.7.Final`.  
#### Installation de Autoconf
Cet outil permet de configurer le code source et le makefile. Il est aussi disponible dans le repo.  
```bash
apt-get install -y autoconf
```
#### Installation de Libtool
Cet outil permet de créer les librairies.
```bash
apt-get install -y libtool
```
#### Installation de Git
```bash
apt-get install -y git
```
#### Installation de Apache2 Dev
Pour compiler le module `mod_cluster` il y a des dépendances. J'installe donc Apache 2 dev qui contient ces dépendances.  
```bash
apt-get install -y apache2-dev
```

### Checkout Sources
Afin de récupèrer les sources on clone le repo git. Executer la commande a l'emplacement ou vous désirez charger les sources.
Après avoir fais le clone on charge le tag `1.3.7.Final`.  
*Pour information il est possible de lister les tag a l'aide la commande `git tag -l`*
```git
git clone https://github.com/modcluster/mod_cluster.git
git checkout tags/1.3.7.Final -b 1.3.7.Final
git checkout tags/1.2.6.Final -b 1.2.6.Final
```
### Configuration et build du module mod_advertise
Le module advertise se trouve dans `native/advertise/`  
```bash
cd native/advertise/
./buildconf
./configure --with-apxs=/usr/bin/apxs
make
cp mod_advertise.so /usr/lib/apache2/modules/
```
L'ajout du module dans le fichier de chargement  
```bash
echo "LoadModule advertise_module /usr/lib/apache2/modules/mod_advertise.so" >> /etc/apache2/mods-available/proxy_cluster.load
```
### Configuration et build du module mod_manager
Le module advertise se trouve dans `native/mod_manager/`  
```bash
cd ../mod_manager/
./buildconf
./configure --with-apxs=/usr/bin/apxs
make
cp mod_manager.so /usr/lib/apache2/modules/
```
L'ajout du module dans le fichier de chargement  
```bash
echo "LoadModule manager_module /usr/lib/apache2/modules/mod_manager.so" >> /etc/apache2/mods-available/proxy_cluster.load
```
### Configuration et build du module mod_proxy_cluster
Le module advertise se trouve dans `native/mod_proxy_cluster/`  
```bash
cd ../mod_proxy_cluster/
./buildconf
./configure --with-apxs=/usr/bin/apxs
make
cp mod_proxy_cluster.so /usr/lib/apache2/modules/
```
L'ajout du module dans le fichier de chargement  
```bash
echo "LoadModule  proxy_cluster_module /usr/lib/apache2/modules/mod_proxy_cluster.so" >> /etc/apache2/mods-available/proxy_cluster.load
```
### Configuration et build du module mod_cluster_slotmem
Le module advertise se trouve dans `native/mod_cluster_slotmem/`  
```bash
cd ../mod_cluster_slotmem/
./buildconf
./configure --with-apxs=/usr/bin/apxs
make
cp mod_cluster_slotmem.so /usr/lib/apache2/modules/
```
L'ajout du module dans le fichier de chargement  
```bash
echo "LoadModule  cluster_slotmem_module /usr/lib/apache2/modules/mod_cluster_slotmem.so" >> /etc/apache2/mods-available/proxy_cluster.load
```
### configuration du mod_cluster dans Apache2
Le fichier de chargement `/etc/apache2/mods-available/proxy_cluster.load` est existant et devrait contenir :
```bash
LoadModule advertise_module /usr/lib/apache2/modules/mod_advertise.so
LoadModule manager_module /usr/lib/apache2/modules/mod_manager.so
LoadModule proxy_cluster_module /usr/lib/apache2/modules/mod_proxy_cluster.so
LoadModule  cluster_slotmem_module /usr/lib/apache2/modules/mod_cluster_slotmem.so
```
Maintenant on ajoute le fichier de configuration `/etc/apache2/mods-available/proxy_cluster.conf` dans lequel on va spécifier la config du module
```bash
MemManagerFile /var/cache/mod_cluster
<IfModule manager_module>
    Listen *:6666
    ManagerBalancerName balancer
   
    <VirtualHost *:6666>
        <Location />
            Require all granted
            Allow from 192.168
            #e.g.: 192.168.0.
        </Location>
   
        KeepAliveTimeout 300
        MaxKeepAliveRequests 0
        AdvertiseFrequency 5
        EnableMCPMReceive On
   
        <Location /mod_cluster_manager>
            SetHandler mod_cluster-manager
            Require all granted
            Allow from 192.168
            #e.g.: 192.168.0.
        </Location>
    </VirtualHost>
</IfModule>
```

## Configuration du mod_cluster



