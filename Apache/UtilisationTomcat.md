# Tomcat

## Utilisation avec un contenaire EJB

### CDI
```xml
<!-- https://mvnrepository.com/artifact/javax.enterprise/cdi-api -->
<dependency>
    <groupId>javax.enterprise</groupId>
    <artifactId>cdi-api</artifactId>
    <version>2.0</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.jboss.weld/weld-core -->
<dependency>
    <groupId>org.jboss.weld</groupId>
    <artifactId>weld-core</artifactId>
    <version>2.4.4.Final</version>
</dependency>
<dependency>
    <groupId>org.jboss.weld.servlet</groupId>
    <artifactId>weld-servlet-shaded</artifactId>
    <version>3.0.1.Final</version>
</dependency>

```
### EJB
```xml
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>${javax.servlet-api.version}</version>
</dependency>

<!-- https://mvnrepository.com/artifact/javax/javaee-api -->
<dependency>
    <groupId>javax</groupId>
    <artifactId>javaee-api</artifactId>
    <version>7.0</version>
</dependency>

```
### JPA
```xml
<dependency>
    <groupId>org.eclipse.persistence</groupId>
    <artifactId>eclipselink</artifactId>
    <version>${eclipselink.version}</version>
</dependency>
<dependency>
    <groupId>org.eclipse.persistence</groupId>
    <artifactId>org.eclipse.persistence.jpa.modelgen.processor</artifactId>
    <version>${eclipselink.version}</version>
</dependency>
```
### JTA
```xml
<!-- https://mvnrepository.com/artifact/org.jboss.spec.javax.transaction/jboss-transaction-api_1.2_spec -->
<dependency>
    <groupId>org.jboss.spec.javax.transaction</groupId>
    <artifactId>jboss-transaction-api_1.2_spec</artifactId>
    <version>1.0.1.Final</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.jboss.transaction/jboss-jta -->
<dependency>
    <groupId>org.jboss.transaction</groupId>
    <artifactId>jboss-jta</artifactId>
    <version>4.2.3.SP7</version>
</dependency>

```
### JMS
```xml
<!-- https://mvnrepository.com/artifact/org.apache.activemq/activemq-core -->
<dependency>
    <groupId>org.apache.activemq</groupId>
    <artifactId>activemq-core</artifactId>
    <version>5.7.0</version>
</dependency>
```
### JAX-RS
```xml
<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>resteasy-jaxrs</artifactId>
    <version>${resteasy.version}</version>
</dependency>

<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>resteasy-jackson2-provider</artifactId>
    <version>${resteasy.version}</version>
</dependency>


<!-- Integration interface in Servlet 3.0 containers to initialize an application -->
<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>resteasy-servlet-initializer</artifactId>
    <version>${resteasy.version}</version>
</dependency>

<!-- Integration with CDI -->
<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>resteasy-cdi</artifactId>
    <version>${resteasy.version}</version>
</dependency>
```

```xml
<dependency>
    <groupId>org.apache.tomee</groupId>
    <artifactId>tomee-catalina</artifactId>
    <version>7.0.3</version>
</dependency>
```

# Lien utiles
[JPA_Tomcat]: http://wiki.eclipse.org/EclipseLink/Examples/JPA/Tomcat_Web_Tutorial "tuo JTa pour tomcat"
[Using JPA/JTA][JPA_Tomcat]