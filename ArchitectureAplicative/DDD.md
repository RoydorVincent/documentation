# Domain Driven Design
DDD est l'acronym de **D**omain **D**riven **D**esign
Le but est de retranscrire le domaine métier dans le code source de l'application.
Il faut donc que toute l'équipe utilise les termes métiers et les utiliser dans le code source.

## les couches du modèle  
* Interface Utilisateur (Presentation)
* Service Applicatif (Application)
* Domaine (Domain)
* Infrastructure
### Presentation
Cette couche a pour responsabilité :  
* Présentation des informations
* Gérer les actions permettant l'interaction avec le système

### Application
A le rôle d'orchestrateur mais ne possède pas de logique métier.  
Exemple: il récupère les objet du domaine et les demande l'action métier sur ces derniers.  
Cette couche a pour responsabilité :  
* Coordonner les activités de l'application
* Gestion de la sécurité
* Gestion de la transaction
* Gestion de session
* Interaction avec les repositories

### Domain
Cette couche gère les éléments du domaine métier. Se sont les éléments qui sont représentatif pour l'utilisateur final.  
Par exemple, pour une application de réservation de billet d'avion, nous trouverons les élément is personne prennent un avion réel.  
Nous y retrouvons:  
* les aggregates
* les spécifications
* les services du domaine

### Infrastructure
Cette couche est une couche de service. on y trouve les principe suivant :  
* l'anti-corruption
* l'isolation de la complexité technique
* la persistance des données du modèle
* la communication entre les différentes couches
* les communication avec les éléments extérieur

#### Anti-Corruption
L'anti-corruption permet de ne pas lier des modèle exterieur avec notre domaine. Par exemple un objet json d'un service REST externe.  

## les éléments du modèle
* Les Aggregate Roots
* Les services du domaine, représentant des opérations agissant sur plusieurs objets (ex: une opération bancaire)
* Les repositories, abstraction du moyen de stockage des Aggregate Roots
* Les specifications, qui représentent des ensembles de critères et permettent d’exprimer des règles métier
* Les évènements du domaine, qui matérialisent des évènements importants survenus dans le domaine

### Aggregate Roots
Représente une agrégation des objets du modèle qui forme une unité cohérente.  
par Exemple,  
pour le cas suivant :  
![ShemaArchiServeurs](resources/DDD/ExempleVoiture.png)  
l'aggregate root `voiture` regroupe les objets `moteur`, `carrosserie`, `roue`  
On peux envisager un agrgrate comme suit :  

```java
public class Car {
    private int number;
    private Double speed;
    
    private Engine      engine;
    private Carrosserie carrosserie;
    private List<Wheel>  wheel;
    
    public void accelerate() throws AccelerationNotAuthorizedException {
        if(AccelerationAuthorizeSpecification.isValidateBy(this)){
            speed += AccelerationCalculationSpecification.calculateSpeed(this);
            DomainEvent.notify(new CarAcceleratedEvent(this.number, this.speed));
        } else {
            throw new AccelerationNotAuthorizedException(this);
        }
    }
}
```

Dans cet exemple, il n'y a pas de couplage sur des mechanism technique.  
2 spécification sont utilisées  
* `AccelerationAuthorizeSpecification`      : contient les règle métier de validation de l'accélération  
* `AccelerationCalculationSpecification`    : contient les règle métier permettant de définir le taux d'accélération de la voiture.    

Cette aproche permet d'isoler les règles métier et de les rendre réutilisable.  
A la fin du tratement métier un événement est envoyé. Celà permet a tout ls abonné d'être avertis du changement via un bus, sur le même principe que le pattern _Observer - Observable_  
L'inconvénient majeur d'une approche evenmentiel est l'augementation de la difficulté de compréhension du modèle  


### Domain Service
Se sont les orchestrateurs. Ils permettent de faire des opération sur plusieur éléments.  

### Repositories
Permet de persiter les éléments du domaine ou de les charger à partir des éléments persisté.  

### Specifications
Regroupe les règles métier sur une unité métier.  

### Domain Event
Représente les événements des notifications des changements lié au domaine.  


# Idée de conception
* Est-il possible d'avoir des implémentation standard dans l'infrastructure => gestionnaire d'evenement (Kafka ou equivalent)
* Est-il possible de créer un archeotype avec des module(mais pas  des modules maven) : module Persistance => ajoute les fichier qui vont bien.

# Bibliographies
https://msdn.microsoft.com/en-us/magazine/dd419654.aspx
https://blog.octo.com/domain-driven-design-des-armes-pour-affronter-la-complexite/