# CQRS
CQRS acronyme de **C**ommande **Q**uery **R**esponsibility **S**egregation  
Le pattern CQRS repose sur la séparation des composants de traitement métier de l’information spécifié par les aspects `command` 
et de restitution de l’information spécifié par les aspect `query`.

Nous avons 2 aspects :
* `command` qui engendre des écritures
* `query` qui engendre des lectures

Ayant 2 flux distinct il est possible de spécialiser les systèmes de stockage pour chaque aspect.

## Query
Cette séparation permet d'avoir un service de lecture qui ne nécessite pas d'élément du modèle.  
Le service de lecture est adapter à la demande de la présentation. De plus aucune modification n'est faite sur le modèle se qui nous permet d'éviter les effets de bord.  
Selon la simplicité de la lecture il est envisageable de ne pas passé par les objet du domaine mais de directement accèder au données en base pour remplir nos DTO.
Toutes fois attention si vous ne passez pas par la couche ORM toutes les facilité de ce dernier ne sont plus présente. Comme par exemple les système de cache.

## Command
La partie commande permet de spécialisé les actions. On peux créer un couple command - handler pour chaque action.  
Si on prend l'approche que les commandes renvois un identifiant de commande afin de lire son status d'execusion, il devient facilement envisageable de concidérer l'execution des commandes par les handler de façon asynchrone.  
On peut aussi permettre le stockage des commandes afin de les tranmetre au handler de façon différé. Peut être utile lors d'un travail en mode hors ligne.

# Bibliographie
https://blog.octo.com/cqrs-larchitecture-aux-deux-visages-partie-1/
https://blog.octo.com/cqrs-larchitecture-aux-deux-visages-partie2/